from gi.repository import Gtk
from .fatfitsync import Fatfit
import datetime

@Gtk.Template(resource_path='/org/gnome/syncer/ui/auth_dialog.ui')
class AuthDialog(Gtk.Dialog):
    __gtype_name__ = 'auth_dialog'
    auth_dialog = Gtk.Template.Child()

    client_id_entry = Gtk.Template.Child()
    client_secret_entry= Gtk.Template.Child()
    response_entry = Gtk.Template.Child()
    response_for = Gtk.Template.Child()
    url_label = Gtk.Template.Child()
    token_label = Gtk.Template.Child()

    ok = Gtk.Template.Child()
    cancel = Gtk.Template.Child()
    accept = Gtk.Template.Child()
    reset = Gtk.Template.Child()

    def __init__(self, parent_win,name, **kwargs):
        super().__init__(**kwargs)

        self.parent = parent_win

        self.auth_dialog.set_transient_for(parent_win)

        self.auth_dialog.connect('delete-event', self.on_escape)

        self.client_id_entry.connect('changed', self.on_entry_not_empty)
        self.client_secret_entry.connect('changed', self.on_entry_not_empty)
        self.client_secret_entry.connect('icon-press', self.on_pass_icon_clicked)
        self.response_entry.connect('changed', self.on_response_not_empty)

        self.accept.connect('clicked', self.on_accept_button_clicked)
        self.reset.connect('clicked' ,self.on_reset_button_clicked)

        self.ok.connect('clicked', self.on_ok_button_clicked)
        self.cancel.connect('clicked', self.on_cancel_button_clicked)

        self.response_for.set_label(str(name))

    def present_cb(self, *args):
        self.auth_dialog.present()

    def on_entry_not_empty(self, entry):
        # if entry.get_text():
        a = self.client_secret_entry.get_text()
        b = self.client_id_entry.get_text()
        if a and b:
            self.accept.set_sensitive(True)
        else:
            self.accept.set_sensitive(False)

    def on_response_not_empty(self, entry):
        if entry.get_text():
            self.ok.set_sensitive(True)
        else:
            self.ok.set_sensitive(False)

    def on_accept_button_clicked(self, *args):
        self.button_logic(False)
        self.client_secret_entry.set_visibility(False)

    def on_reset_button_clicked(self, *args):
        self.button_logic(True)
        self.url_label.set_label('')
        self.response_entry.set_sensitive(False)

    def on_ok_button_clicked(self, *args):
        # self.parent.module[i].sync.set_sensitive(True)

        # if on successful_authorization():
        self.parent.status()
        self.auth_dialog.hide()

    def on_cancel_button_clicked(self, *args):
        self.auth_dialog.hide()

    def on_escape(self, *args):
        self.on_cancel_button_clicked()
        return True

    def button_logic(self, logic):
        self.client_id_entry.set_sensitive(logic)
        self.client_secret_entry.set_sensitive(logic)
        self.accept.set_sensitive(logic)
        self.reset.set_sensitive(not logic)
        # self.response_entry.set_sensitive(not logic)

    def on_pass_icon_clicked(self, entry, pos, *args):
        vis = entry.get_visibility()
        if vis is True:
            entry.set_visibility(False)
            entry.set_icon_from_icon_name(pos , 'view-conceal-symbolic')
        else:
            entry.set_visibility(True)
            entry.set_icon_from_icon_name(pos , 'view-reveal-symbolic')
