# this should be the item used for each connection ui
from gi.repository import Gtk
from .auth_dialog import AuthDialog as auth
from .fatsync2 import Fatsync
from .fitsync2 import Fitsync
from .sync import Sync


class ServiceConnectionItem(Sync, Gtk.Stack):
    def __init__(self, *args, **kwargs):
        self.win = args[0]
        print(self.name)
        self.auth = auth(self.win, self.name)

        super().__init__(*args, **kwargs)

        if self.connected:
            self.on_connect()

        self.authorize_button.connect('clicked', self.on_authorize)
        self.connect_button.connect('clicked', self.on_connect)

        self.auth.accept.connect('clicked', self.receive_client_info)
        self.auth.accept.connect('clicked', self.request_permission)

        # self.auth.ok.connect('clicked', self.on_authorization_initiated)
        self.auth.ok.connect('clicked', self.receive_permission)
        self.auth.ok.connect('clicked', self.on_successful_authorization)
        # self.auth.auth_dialog.connect('close', self.on_successful_authorization)

    # def receive_client_info(self, button, state):
    def receive_client_info(self, *args):
        print(*args)
        # print(button)
        # print(state)
        # if state == 'GTK_STATE_INSENSITIV':
        #     print('aaa',state)
        self.client_id = self.auth.client_id_entry.get_text()
        self.client_secret = self.auth.client_secret_entry.get_text()
        self.write()

    # @self.fs.request_permission
    def request_permission_url(self, url,*args):
        print(args)
        print(url)
        self.auth.url_label.set_label(
            '<a href="{}">Visit</a>'.format(
                str(url).replace('&','&amp;'))
            )
        self.auth.response_entry.set_sensitive(True)

    # receive_permission_code
    def receive_code(self):
        return self.auth.response_entry.get_text()

    # receive_permission_url
    def receive_url(self):
        return self.auth.response_entry.get_text()

    def update_dialog(self):
        cid = self.client_id
        csec = self.client_secret
        if cid and csec and self.connected:
            self.auth.client_id_entry.set_text(str(cid))
            self.auth.client_secret_entry.set_text(str(csec))
            self.auth.on_accept_button_clicked()
            self.auth.token_label.set_label(str(self.token))
            # self.auth.response_entry.set_sensitive(False)

    def on_authorization_initiated(self, *args):
        pass

    def on_successful_authorization(self, *args):
        print('ok button logic applied')
        self._button_logic(False)
        self.write()

    def on_disconnected(self):
        self.win.notify_msg(' '.join((self.title, 'not connected')))
        self.win.notify()
        # self.on_authorize()

    def on_authorize(self, *args):
        self.update_dialog()
        self.auth.present_cb()
        # self.auth.auth_dialog.present()

    def on_connect(self, *args):
        # self.connection()
        # self.connected = False
        print(self,self.connected)
        if self.connected:
            print('whooo connected')
            # print(self.token)
            self._button_logic(False)
        else:
            self._button_logic(True)

    def _button_logic(self, logic):
        self.authorize_button.set_sensitive(logic)
        self.connect_button.set_sensitive(not logic)


@Gtk.Template(resource_path='/org/gnome/syncer/ui/sync_connection_item.ui')
class ServiceA(Fatsync, ServiceConnectionItem):
    __gtype_name__ = 'FatsyncConnectionItem'
    sync_connection_item = Gtk.Template.Child()

    authorize_button = Gtk.Template.Child()
    connect_button = Gtk.Template.Child()

    name = "fs"
    title = "FatSecret"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


@Gtk.Template(resource_path='/org/gnome/syncer/ui/sync_connection_item.ui')
class ServiceB(Fitsync, ServiceConnectionItem):
    __gtype_name__ = 'FitsyncConnectionItem'
    sync_connection_item = Gtk.Template.Child()

    authorize_button = Gtk.Template.Child()
    connect_button = Gtk.Template.Child()

    name = "fit"
    title = "Fitbit"

    def __init__(self, parent_win, **kwargs):
        super().__init__(parent_win, **kwargs)
