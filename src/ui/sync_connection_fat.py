from gi.repository import Gtk
from .auth_dialog import AuthDialog as auth
from .fatsync2 import Fatsync

@Gtk.Template(resource_path='/org/gnome/syncer/ui/sync_connection_item.ui')
class Fatservice(Fatsync, Gtk.Stack):
    # __gtype_name__ = ''
    pass
