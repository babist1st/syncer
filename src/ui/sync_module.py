from gi.repository import Gtk, GObject
from .sync_connection import ServiceA, ServiceB
from .fatfitsync import Fatfit, FF
import datetime, threading


class SyncModule(FF, Gtk.Stack):
    def __init__(self, parent_win):
        print(self.__class__.__bases__)
        self.connected = False
        self.win = parent_win
        super().__init__()

        # self.sync_grid.set_child(parent_win)
        # self.prep.connect('clicked', self.on_sync)
        self.sync.connect('clicked', self.on_sync)

        self.calendar.connect('month-changed', self.unmark_today)
        self.calendar.connect('day-selected', self.update_day)
        self.connection_status()
        self.init_date()
        self.add_con_to_stack()

    def button_logic(self, logic):
        self.sync.set_sensitive(True)
        self.buttons_on_connected()

    def connection_status(self,*args):
        super().connection_status()
        if self.connected:
            self.button_logic(True)

    @FF.loop_connections
    def buttons_on_connected(self, con):
        con.authorize_button.set_sensitive(True)
        con.connect_button.set_sensitive(False)

    @FF.loop_connections
    def add_con_to_stack(self, con):
        self.con_stack.add_titled(con.sync_connection_item, con.name, con.title)
        con.auth.ok.connect('clicked', self.connection_status)
        #con.auth.auth_dialog.connect('hide', self.connection_status)

    def check(self):
        if not self.running:
            self.stop_progress()
            # self.pbar.pulse()   # or set_fraction, etc.
        return self.running

    def on_sync(self, *args):
        try:
            self.running = True
            self.on_sync_button_logic(False)
            self.work_thread = threading.Thread(target = self.update )
            GObject.timeout_add(200, self.check)
            self.work_thread.start()
        except Exception as e:
            print(e)
            self.notify_msg(str(e))

    def stop_progress(self):
        self.running = False
        self.work_thread.join()
        self.on_sync_button_logic(True)

    def on_sync_button_logic(self, val):
        self.calendar.set_sensitive(val)
        self.sync.set_sensitive(val)

    def notify_msg(self, msg, *args):
        # print(msg,*args)
        # iter = self.output.insert(-1, [msg])
        # print(iter)
        iter = self.output.prepend([' '.join((msg,str(*args)))])
        # print (iter)

        self.win.notify_msg(msg=msg)
        self.win.notify()

    def update_day(self, *args):
        select_day = self.calendar.get_date()
        self.date = datetime.date(select_day[0], select_day[1]+1, select_day[2])
        self.day = datetime.datetime(select_day[0], select_day[1]+1, select_day[2])
        # self.day = datetime.datetime.combine(select_day, datetime.datetime.min.time())
        # print(self.day, self.day.date())

    def init_date(self):
        self.day = datetime.date.today()
        self.today = datetime.date.today()
        # self.calendar.select_month(self.today.month-1, self.today.year)
        self.calendar.select_month(self.today.month-1, self.today.year)
        self.calendar.select_day(self.today.day)
        self.calendar.mark_day(self.today.day)

    def unmark_today(self, calendar):
        select_day = calendar.get_date()
        if self.today.month == select_day[1]+1 and self.today.year == select_day[0]:
            calendar.mark_day(self.today.day)
        else:
            calendar.unmark_day(self.today.day)

    def get_selected_connection(self):
        select_con_name = self.con_stack.get_visible_child_name()
        if select_con_name != 'data':
            return self.connections[select_con_name]

    def progress(self, percent):
        self.pbar.set_fraction(percent/100)

    def passme(self):
        pass


@Gtk.Template(resource_path='/org/gnome/syncer/ui/sync_module2.ui')
class ModuleA(SyncModule, Fatfit):
#    __gtype_name__ = 'f2f_sync_module'
    __gtype_name__ = 'F2fSyncModule'
    sync_module = Gtk.Template.Child()

    calendar = Gtk.Template.Child()

    pbar = Gtk.Template.Child()
    sync = Gtk.Template.Child()
    # prep = Gtk.Template.Child()
    con_stack = Gtk.Template.Child()
    con_switcher = Gtk.Template.Child()
    output = Gtk.Template.Child()

    name = "f2f"
    title = "Fat2Fit"

    def __init__(self, parent_win):
        super().__init__(parent_win)

    def setup_connections(self):
        self.connections = {
            "fs":ServiceA(self.win),
            "fit":ServiceB(self.win)
            }
