# window.py
#
# Copyright 2020 Babis Trapalis
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
from .sync_module import ModuleA

@Gtk.Template(resource_path='/org/gnome/syncer/window.ui')
class SyncerWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'Syncer'

    delete = Gtk.Template.Child()

    calendar_stack = Gtk.Template.Child()
    calendar_button = Gtk.Template.Child()

    con_switcher_collector_stack = Gtk.Template.Child()

    config_button = Gtk.Template.Child()
    choice_revealer = Gtk.Template.Child()

    notify_label = Gtk.Template.Child()
    notify_button = Gtk.Template.Child()
    revealer = Gtk.Template.Child()

    module_button = Gtk.Template.Child()
    module_stack = Gtk.Template.Child()
    module_switcher = Gtk.Template.Child()
    module_revealer = Gtk.Template.Child()

    status_bar = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # super().__init__(application = application, **kwargs)

        self.app = kwargs['application']

        self.calendar_button.connect('clicked',self.on_button)
        self.delete.connect('clicked', self.delete_data)
        self.delete.connect('clicked', self.notify)
        self.notify_button.connect('clicked', self.on_notify_button)

        self.module_button.connect('clicked', self.on_reveal, self.module_revealer)
        self.config_button.connect('clicked', self.on_reveal, self.choice_revealer)

        self.modules = {
            # "ff":ModuleA(self),
            "f2f":ModuleA(self)
            # 'sc', 'stravaCalSync')
            }

        # for mod in self.modules.values():
        for key, mod in self.modules.items():
            self.module_stack.add_titled(mod.sync_module, key, mod.title)
            self.con_switcher_collector_stack.add_titled(mod.con_switcher, key, mod.title)
            self.calendar_stack.add_titled(mod.calendar, key, mod.title)

            # mod.sync.connect('released', self.notify)

        self.module_stack.connect('notify::visible-child', self.change_con_stack)

        self.status()

        # stack.active.sync.connect['clciked',notify]

    def on_button(self, button):
        popover = button.get_popover()
        popover.grab_focus()

    def on_reveal(self, button, revealer):
        if revealer.get_reveal_child():
            revealer.set_reveal_child(False)
        else:
            revealer.set_reveal_child(True)

    def change_con_stack(self, *args):
        select_mod_name = self.module_stack.get_visible_child_name()
        self.con_switcher_collector_stack.set_visible_child_name(select_mod_name)
        self.calendar_stack.set_visible_child_name(select_mod_name)

    def get_selected_module(self):
        select_mod_name = self.module_stack.get_visible_child_name()
        return self.modules[select_mod_name]

    def delete_data(self, *args):
        select_mod = self.get_selected_module()
        select_con = select_mod.get_selected_connection()
        if not select_con:
            self.notify_msg(msg='Wrong selection')
        else:
            self.notify_msg(msg=''.join(("Deleted config: ", select_con.title)))
            select_con.delete()

    def notify(self, *args):
        val = True
        if self.revealer.get_reveal_child():
            val = False
            self.revealer.set_transition_duration(0)
            self.revealer.set_reveal_child(val)
            # self.notify(msg = msg)
            # self.revealer.set_reveal_child(True)
            # self.notify_label.set_label(msg)
        # elif not self.revealer.get_child_revealed():
        self.revealer.set_transition_duration(300)
        self.revealer.set_reveal_child(True)
        # print(self.revealer.has_focus())

    def notify_msg(self, msg='None'):
        self.notify_label.set_label(msg)

    #TODO: Implement a notification system
    def on_notify_button(self, *args):
        self.revealer.set_reveal_child(False)

    def set_status(self):
        pass

    #TODO: Implement a status system with history
    def status(self):
        for mod in self.modules.values():
            # self.mod.sync.set_sensitive(True)
            for key, con in mod.connections.items():
                print(con)
                context_id = self.status_bar.get_context_id(key)
                # self.status_bar.push(context_id, "What?")
                if con.connected:
                    self.status_bar.push(context_id, "Connected")
                else:
                    self.status_bar.push(context_id, "Disconnected")
